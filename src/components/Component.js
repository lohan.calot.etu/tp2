export default class Component {
	tagName;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	renderAttribute() {
		return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}" />`;
	}
	render() {
		if (!this.children) return this.renderAttribute();
		return `<${this.tagName}>${this.renderChildren()}</${this.tagName}>`;
	}
	renderChildren() {
		if (this.children instanceof Array) {
			let res = ``;
			for (let i = 0; i < this.children.length; i++) {
				if (this.children[i] instanceof Component) {
					res += this.children[i].render();
				} else {
					res += this.children[i];
				}
			}
			return res;
		} else {
			if (this.children instanceof Component) return this.children.render();
			else return this.children;
		}
	}
}
