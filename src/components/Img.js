import Component from './Component.js';
export default class Img extends Component {
	source;
	constructor(source) {
		super('img', { name: 'src', value: source }, '');
		this.source = source;
	}
}
